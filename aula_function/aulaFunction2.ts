namespace aulaFunction2 {
    function multiplicarPor(n:number):(x:number) => number {
        return function (x:number):number {
          return x * n;
        }
    }
    let duplicar = multiplicarPor(2);
    let resultado = duplicar(5);
    console.log(resultado);
    let triplicar = multiplicarPor(3);
    resultado = triplicar(5)
}