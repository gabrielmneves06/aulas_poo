namespace exercicio_3 {
  let livros: any[] = [
    { titulo: "Senhor dos Anéis", autor: "R.R asdad" },
    { titulo: "Harry Potter", autor: "J.K assdas" },
    { titulo: "Mulan", autor: "Disney" },
    { titulo: "Mickey", autor: "Disney" },
    { titulo: "Donald", autor: "Disney" },
  ];

  let titulos = livros.map((livro) => {
    return livro.titulo;
  });
  let autores = livros.map((livro) => {
    return livro.autor;
  });
  console.log(titulos);
  console.log(autores);

  let autor3 = livros.filter((livro) => {
    return livro.autor === "Disney";
  });
  console.log(autor3);

  let titulosAutor3 = autor3.map((livro) => {
    return livro.titulo;
  });
  console.log(titulosAutor3);
}
