interface Aluno {
    nome: string;
    idade: number;
    notas: number[];
    }
namespace exercicio_6 {
    const alunos: Aluno[] = [
        {nome: "Aluno 1", idade: 20, notas:[4, 7, 8]},
        {nome: "Aluno 2", idade: 21, notas:[2, 8, 9]},
        {nome: "Aluno 3", idade: 23, notas:[9, 5, 7]},
        {nome: "Aluno 4", idade: 19, notas:[6, 3, 7]},
        {nome: "Aluno 5", idade: 24, notas:[8, 7, 8]},
        {nome: "Aluno 6", idade: 20, notas:[9, 9, 5]},
    ]

    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce(
            (total, nota) => {return total + nota}
        ) / aluno.notas.length
        if (media >= 6) {
            console.log(`A media do aluno: ${aluno.nome} e igual ${media} e esta aprovado`);
        } else {
            console.log(`A media do aluno: ${aluno.nome} e igual ${media} e esta reprovado`);
            
        }
    })
}
