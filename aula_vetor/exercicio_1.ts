namespace exercicio_1 {
    let numeros: number[] = [10, 8, 6, 4, 2];
    let desc: string[] = ["Numero 1", "Numero 2", "Numero 3", "Numero 4", "Numero 5"];

    let somaNumeros: number = 0;
    for (let i = 0; i < numeros.length; i++) {
        somaNumeros += numeros[i];
    }

    let result: number;
    result = somaNumeros;
    for (let i = 0; i < numeros.length; i++) {
        console.log(`${desc[i]} = ${numeros[i]}`);
    }
    
    console.log(`A soma dos numeros é: ${result}`);
}